package cz.muni.fi.pv256.movio.androidproject436351.activity;

import android.os.PersistableBundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import cz.muni.fi.pv256.movio.androidproject436351.R;
import cz.muni.fi.pv256.movio.androidproject436351.fragment.FilmDetailFragment;
import cz.muni.fi.pv256.movio.androidproject436351.model.Film;

public class FilmDetailActivity extends AppCompatActivity {

    public static final String EXTRA_FILM = "extra_film";

    private Film mFilm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_detail);

        initData(savedInstanceState);
        addFilmDetailFragment(mFilm);
    }

    private void initData(Bundle bundle) {
        if (bundle == null) {
            mFilm = getIntent().getParcelableExtra(EXTRA_FILM);
        }
    }

    private void addFilmDetailFragment(Film film) {
        FragmentManager fm = getSupportFragmentManager();
        FilmDetailFragment fragment = (FilmDetailFragment) fm.findFragmentById(R.id.activity_film_detail_fragment_container);

        if (fragment == null) {
            fragment = FilmDetailFragment.newInstance(film);
            fm.beginTransaction().add(R.id.activity_film_detail_fragment_container, fragment).commit();
        }
    }
}

