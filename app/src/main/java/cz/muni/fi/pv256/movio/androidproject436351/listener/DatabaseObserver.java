package cz.muni.fi.pv256.movio.androidproject436351.listener;

/**
 * Created by Juraj on 1/2/2016.
 */
public interface DatabaseObserver {
    void onDatabaseDataChanged();
}
