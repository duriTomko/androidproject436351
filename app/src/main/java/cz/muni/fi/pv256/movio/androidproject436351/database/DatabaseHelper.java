package cz.muni.fi.pv256.movio.androidproject436351.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Juraj on 12/8/2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = DatabaseHelper.class.getSimpleName();

    private static final String DB_NAME = "film.database";
    private static final int DB_VERSION = 1;

    private static final String CREATE_TABLE =
                    "create table " + DbSchema.FilmTable.NAME + "(" +
                    DbSchema.FilmTable.Cols.ID + " integer primary key," +
                    DbSchema.FilmTable.Cols.RELEASE_DATE + " text," +
                    DbSchema.FilmTable.Cols.POSTER_PATH + " text," +
                    DbSchema.FilmTable.Cols.TITLE + " text," +
                    DbSchema.FilmTable.Cols.LANGUAGE + " text," +
                    DbSchema.FilmTable.Cols.BACKDROP_PATH + " text," +
                    DbSchema.FilmTable.Cols.IS_FAVOURITE + " integer," +
                    DbSchema.FilmTable.Cols.OVERVIEW + " text );";

    private static final String DROP_TABLE = "drop table if exists " + DbSchema.FilmTable.NAME;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE);
        onCreate(db);
    }
}
