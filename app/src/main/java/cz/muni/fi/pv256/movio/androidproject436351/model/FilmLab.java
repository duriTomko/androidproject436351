package cz.muni.fi.pv256.movio.androidproject436351.model;

import android.content.Context;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import cz.muni.fi.pv256.movio.androidproject436351.R;

/**
 * Created by Juraj on 10/18/2015.
 */
public class FilmLab {

    private static FilmLab sFilmLab;
    private ArrayList<Film> mFilmList = new ArrayList<>();

    private FilmLab() {
    }

    public static FilmLab get() {
        if (sFilmLab == null) {
            sFilmLab = new FilmLab();
        }
        return sFilmLab;
    }

    public ArrayList<Film> getFilmList() {
        return mFilmList;
    }

    public void setFilmList(ArrayList<Film> filmList) {
        mFilmList = filmList;
    }

    public void setFavorite(long id, boolean isFavorite) {
        for (Film item : mFilmList) {
            if (item.getId() == id) {
                item.setIsFavorite(isFavorite);
            }
        }
    }

    public Film getFilm(long id) {
        for (Film item : mFilmList) {
            if (item.getId() == id) {
                return item;
            }
        }
        return null;
    }
}
