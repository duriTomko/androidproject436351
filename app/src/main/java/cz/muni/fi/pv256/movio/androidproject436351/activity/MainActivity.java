package cz.muni.fi.pv256.movio.androidproject436351.activity;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cz.muni.fi.pv256.movio.androidproject436351.adapter.MainActivityPagerAdapter;
import cz.muni.fi.pv256.movio.androidproject436351.fragment.DiscoverFragment;
import cz.muni.fi.pv256.movio.androidproject436351.model.FilmLab;
import cz.muni.fi.pv256.movio.androidproject436351.receiver.DownloadReceiver;
import cz.muni.fi.pv256.movio.androidproject436351.service.DownloadService;
import cz.muni.fi.pv256.movio.androidproject436351.sync.UpdaterSyncAdapter;
import cz.muni.fi.pv256.movio.androidproject436351.task.GetFilmsTask;
import cz.muni.fi.pv256.movio.androidproject436351.R;
import cz.muni.fi.pv256.movio.androidproject436351.fragment.FilmDetailFragment;
import cz.muni.fi.pv256.movio.androidproject436351.fragment.base.MainFragment;
import cz.muni.fi.pv256.movio.androidproject436351.model.Film;
import cz.muni.fi.pv256.movio.androidproject436351.util.NotifyUtil;
import cz.muni.fi.pv256.movio.androidproject436351.view.SlidingTabLayout;

public class MainActivity extends AppCompatActivity implements MainFragment.OnFilmPickListener,
        GetFilmsTask.GetFilmsTaskListener,
        DownloadReceiver.Receiver {

    private FrameLayout mFilmDetailFragmentContainer;
    private GetFilmsTask mGetFilmsTask;
    private FrameLayout mSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        UpdaterSyncAdapter.initializeSyncAdapter(this);

        setToolbar();
        setViewPager();

        mFilmDetailFragmentContainer = (FrameLayout) findViewById(R.id.fragment_detail_container);
        mSpinner = (FrameLayout) findViewById(R.id.spinner_layout);
        //addMainFragment();

        downloadFilmsData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mGetFilmsTask != null) {
            mGetFilmsTask.cancel(false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sync:
                UpdaterSyncAdapter.syncImmediately(this);
                Toast.makeText(this, getString(R.string.sync_started), Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void setViewPager() {
        CharSequence[] titles = new CharSequence[] { getString(R.string.fragment_discover), getString(R.string.fragment_favorites) };
        MainActivityPagerAdapter pagerAdapter = new MainActivityPagerAdapter(getSupportFragmentManager(), titles);
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setAdapter(pagerAdapter);

        SlidingTabLayout tabLayout = (SlidingTabLayout) findViewById(R.id.slidingTabs);
        tabLayout.setDistributeEvenly(true);
        tabLayout.setViewPager(viewPager);
    }

    private void downloadFilmsData() {
        DownloadReceiver receiver = new DownloadReceiver(new Handler());
        receiver.setReceiver(this);
        Intent intent = DownloadService.newIntent(this, receiver);
        startService(intent);
        showSpinner();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case DownloadService.STATUS_OK:
                if (resultData != null) {
                    ArrayList<Film> films = resultData.getParcelableArrayList(DownloadService.EXTRA_FILMS);
                    FilmLab.get().setFilmList(films);
                    notifyFilmsDataChanged(films);
                }
                break;
            case DownloadService.STATUS_NETWORK_NOT_AVAILABLE:
                NotifyUtil.sendNotification(getResources().getString(R.string.error_downloading),
                        getResources().getString(R.string.notification_network_not_available));
                break;
            case DownloadService.STATUS_ERROR:
                NotifyUtil.sendNotification(getResources().getString(R.string.error_downloading),
                        getResources().getString(R.string.notification_error));
                break;
        }
        dissmissSpinner();
    }

    private boolean isTabletDevice() {
        double size = 0;
        try {
            // Compute screen size
            DisplayMetrics dm = getResources().getDisplayMetrics();
            float screenWidth = dm.widthPixels / dm.xdpi;
            float screenHeight = dm.heightPixels / dm.ydpi;
            size = Math.sqrt(Math.pow(screenWidth, 2) + Math.pow(screenHeight, 2));

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (size >= 7) {
            return true;
        } else {
            return false;
        }
    }

    private void addFilmDetailFragment(Film film) {
        FragmentManager fm = getSupportFragmentManager();
        FilmDetailFragment fragment = FilmDetailFragment.newInstance(film);

        if (fragment == null) {
            fm.beginTransaction().add(R.id.fragment_detail_container, fragment).commit();
        } else {
            fm.beginTransaction().replace(R.id.fragment_detail_container, fragment).commit();
        }
        mFilmDetailFragmentContainer.setVisibility(View.VISIBLE);
    }

    private void startFilmDetailActivity(Film film) {
        Intent intent = new Intent(this, FilmDetailActivity.class);
        intent.putExtra(FilmDetailActivity.EXTRA_FILM, film);
        startActivity(intent);
    }

    private void notifyFilmsDataChanged(ArrayList<Film> filmList) {
        List<Fragment> fragments = getFragments();
        if (fragments == null) return;

        for (Fragment fragment : fragments) {
            if (fragment instanceof DiscoverFragment) {
                ((DiscoverFragment) fragment).setFilmsData(filmList);
            }
        }
    }

    private List<Fragment> getFragments() {
        FragmentManager fm = getSupportFragmentManager();
        List<Fragment> fragments = fm.getFragments();
        return fragments;
    }

    private void showSpinner() {
        mSpinner.setVisibility(View.VISIBLE);
    }

    private void dissmissSpinner() {
        mSpinner.setVisibility(View.GONE);
    }

    @Override
    public void onFilmPick(Film film) {
        if (isTabletDevice()) {
            addFilmDetailFragment(film);
        } else {
            startFilmDetailActivity(film);
        }
    }

    @Override
    public void onFilmsDownloaded(ArrayList<Film> filmList) {
        notifyFilmsDataChanged(filmList);
    }
}
