package cz.muni.fi.pv256.movio.androidproject436351.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;

import cz.muni.fi.pv256.movio.androidproject436351.R;
import cz.muni.fi.pv256.movio.androidproject436351.app.App;

/**
 * Created by Juraj on 1/3/2016.
 */
public class NotifyUtil {

    public static void sendNotification(String title, String content) {
        Context appContext = App.getAppContext();

        Notification n = new Notification.Builder(appContext)
                .setContentTitle(title)
                .setContentText(content)
                .setSmallIcon(R.drawable.ic_account_small)
                .setAutoCancel(true)
                .build();

        NotificationManager notificationManager =
                (NotificationManager) appContext.getSystemService(appContext.NOTIFICATION_SERVICE);
        notificationManager.notify(0, n);
    }
}
