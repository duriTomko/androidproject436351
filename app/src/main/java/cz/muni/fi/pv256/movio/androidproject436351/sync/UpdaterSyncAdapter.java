package cz.muni.fi.pv256.movio.androidproject436351.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncRequest;
import android.content.SyncResult;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cz.muni.fi.pv256.movio.androidproject436351.R;
import cz.muni.fi.pv256.movio.androidproject436351.client.RestApi;
import cz.muni.fi.pv256.movio.androidproject436351.database.DatabaseManager;
import cz.muni.fi.pv256.movio.androidproject436351.model.Film;
import cz.muni.fi.pv256.movio.androidproject436351.util.DateDeserializer;
import cz.muni.fi.pv256.movio.androidproject436351.util.NotifyUtil;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by Juraj on 1/3/2016.
 */
public class UpdaterSyncAdapter extends AbstractThreadedSyncAdapter {
    private static final String TAG = UpdaterSyncAdapter.class.getSimpleName();

    // Interval at which to sync with the server, in seconds.
    public static final int SYNC_INTERVAL = 60 * 60 * 24; //day
    public static final int SYNC_FLEXTIME = SYNC_INTERVAL / 3;

    private static final String API_URL = "http://api.themoviedb.org";

    private DatabaseManager mDbManager;

    public UpdaterSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);

        mDbManager = DatabaseManager.getInstance();
    }

    /**
     * Helper method to schedule the sync adapter periodic execution
     */
    public static void configurePeriodicSync(Context context, int syncInterval, int flexTime) {
        Account account = getSyncAccount(context);
        String authority = context.getString(R.string.content_authority);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // we can enable inexact timers in our periodic sync
            SyncRequest request = new SyncRequest.Builder()
                    .syncPeriodic(syncInterval, flexTime)
                    .setSyncAdapter(account, authority)
                    .setExtras(Bundle.EMPTY) //enter non null Bundle, otherwise on some phones it crashes sync
                    .build();
            ContentResolver.requestSync(request);
        } else {
            ContentResolver.addPeriodicSync(account, authority, Bundle.EMPTY, syncInterval);
        }
    }

    /**
     * Helper method to have the sync adapter sync immediately
     *
     * @param context The context used to access the account service
     */
    public static void syncImmediately(Context context) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        Account account = getSyncAccount(context);
        ContentResolver.requestSync(account, context.getString(R.string.content_authority), bundle);
    }

    public static void initializeSyncAdapter(Context context) {
        getSyncAccount(context);
    }

    /**
     * Helper method to get the fake account to be used with SyncAdapter, or make a new one if the
     * fake account doesn't exist yet.  If we make a new account, we call the onAccountCreated
     * method so we can initialize things.
     *
     * @param context The context used to access the account service
     * @return a fake account.
     */
    public static Account getSyncAccount(Context context) {
        // Get an instance of the Android account manager
        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);

        // Create the account type and default account
        Account newAccount = new Account(context.getString(R.string.app_name), context.getString(R.string.sync_account_type));

        // If the password doesn't exist, the account doesn't exist
        if (null == accountManager.getPassword(newAccount)) {

        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
            if (!accountManager.addAccountExplicitly(newAccount, "", null)) {
                return null;
            }
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call ContentResolver.setIsSyncable(account, AUTHORITY, 1)
             * here.
             */

            onAccountCreated(newAccount, context);
        }
        return newAccount;
    }

    private static void onAccountCreated(Account newAccount, Context context) {
        /*
         * Since we've created an account
         */
        UpdaterSyncAdapter.configurePeriodicSync(context, SYNC_INTERVAL, SYNC_FLEXTIME);

        /*
         * Without calling setSyncAutomatically, our periodic sync will not be enabled.
         */
        ContentResolver.setSyncAutomatically(newAccount, context.getString(R.string.content_authority), true);

        /*
         * Finally, let's do a sync to get things started
         */
        syncImmediately(context);
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        Log.d(TAG, "onPerformSync() was called");

        List<Film> localList = mDbManager.readAll();
        ArrayList<Film> remoteList = getRemoteList(localList);
        ArrayList<Film> updatedFilms = getUpdatedFilms(localList, remoteList);
        updateLocalStorage(updatedFilms);
    }

    private ArrayList<Film> getRemoteList(List<Film> localList) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Date.class, new DateDeserializer())
                .create();

        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(API_URL)
                .setConverter(new GsonConverter(gson))
                .build();

        String api_key = getContext().getResources().getString(R.string.movie_db_api_key);
        RestApi methods = adapter.create(RestApi.class);

        ArrayList<Film> remoteList = new ArrayList<>();
        for (Film item : localList) {
            Film response = null;
            try {
                response = methods.getMovieById(item.getId(), api_key);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (response != null) {
                Log.d(TAG, "found film = " + response);
                remoteList.add(response);
            } else {
                Log.d(TAG, "something went wrong");
            }
        }
        return remoteList;
    }

    private ArrayList<Film> getUpdatedFilms(List<Film> localList, List<Film> remoteList) {
        ArrayList<Film> updateList = new ArrayList<>();
        for (int i = 0; i < localList.size(); i++) {
            Film local = localList.get(i);
            Film remote = remoteList.get(i);

            if (!local.equals(remote)) {
                updateList.add(remote);
            }
        }
        return updateList;
    }

    private void updateLocalStorage(ArrayList<Film> films) {
        if (films == null || films.isEmpty()) return;
        for (Film item : films) {
            mDbManager.update(item);
        }

        NotifyUtil.sendNotification(getContext().getString(R.string.movies_sync),
                getContext().getString(R.string.movies_sync_successful));
    }
}