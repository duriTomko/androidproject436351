package cz.muni.fi.pv256.movio.androidproject436351.client;

import cz.muni.fi.pv256.movio.androidproject436351.model.Film;
import cz.muni.fi.pv256.movio.androidproject436351.model.serverresponse.GetFilmByIdResponse;
import cz.muni.fi.pv256.movio.androidproject436351.model.serverresponse.GetFilmsResponse;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by Juraj on 11/30/2015.
 */
public interface RestApi {

    @GET("/3/discover/movie")
    GetFilmsResponse getMovies(@Query("api_key") String apiKey);

    @GET("/3/movie/{id}")
    Film getMovieById(@Path("id") long id, @Query("api_key") String apiKey);
}
