package cz.muni.fi.pv256.movio.androidproject436351.fragment.base;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import cz.muni.fi.pv256.movio.androidproject436351.R;
import cz.muni.fi.pv256.movio.androidproject436351.adapter.FilmAdapter;
import cz.muni.fi.pv256.movio.androidproject436351.model.Film;
import cz.muni.fi.pv256.movio.androidproject436351.util.NetworkUtil;

/**
 * Created by Juraj on 10/26/2015.
 */
public abstract class MainFragment extends Fragment implements FilmAdapter.FilmAdapterListener {
    private static final String TAG = MainFragment.class.getSimpleName();

    private OnFilmPickListener mListener;

    private Context mContext;
    private ArrayList<Film> mFilmList;

    private GridView mGridView;
    private ProgressBar mProgressBar;
    private FilmAdapter mAdapter;

    public interface OnFilmPickListener {
        void onFilmPick(Film film);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (OnFilmPickListener) activity;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        setUpGridView(view);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progress_bar);

        return view;
    }

    public void setFilmsData(ArrayList<Film> filmList) {
        if (filmList == null || filmList.isEmpty()) return;
        mFilmList = filmList;
        setAdapter(filmList);
        Log.d(TAG, "filmList = " + filmList.toString());
        Log.d(TAG, "filmList size = " + filmList.size());
    }

    private void setUpGridView(View rootView) {
        mGridView = (GridView) rootView.findViewById(R.id.fragment_main_grid_view);

        if (mFilmList != null && !mFilmList.isEmpty()) {
            setAdapter(mFilmList);
        } else {
            setEmptyView(rootView);
        }
    }

    private void setEmptyView(View rootView) {
        ViewStub viewStub = (ViewStub) rootView.findViewById(R.id.fragment_main_view_stub);
        View emptyView = viewStub.inflate();
        mGridView.setEmptyView(emptyView);

        TextView noConnectionTv = (TextView) emptyView.findViewById(R.id.empty_view_main_fragment_no_connection_tv);
        TextView noDataTv = (TextView) emptyView.findViewById(R.id.empty_view_main_fragment_no_data_tv);
        if (!NetworkUtil.isNetworkAvailable()) {
            noConnectionTv.setVisibility(View.VISIBLE);
            noDataTv.setVisibility(View.GONE);
        } else {
            noDataTv.setVisibility(View.VISIBLE);
            noConnectionTv.setVisibility(View.GONE);
        }
    }

    private void setAdapter(final ArrayList<Film> filmList) {
        mAdapter = new FilmAdapter(filmList, this, this);
        mGridView.setAdapter(mAdapter);
    }

    protected void updateAdapterData(ArrayList<Film> filmList) {
        if (mAdapter == null) {
            setAdapter(filmList);
        } else {
            mAdapter.setData(filmList);
        }
    }

    @Override
    public void onItemClick(Film film) {
        mListener.onFilmPick(film);
    }

    @Override
    public void onItemLongClick(Film film) {
        Toast.makeText(mContext, film.getTitle(), Toast.LENGTH_SHORT).show();
    }
}
