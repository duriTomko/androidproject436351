package cz.muni.fi.pv256.movio.androidproject436351.model.serverresponse;

import com.google.gson.annotations.SerializedName;

import cz.muni.fi.pv256.movio.androidproject436351.model.Film;

/**
 * Created by Juraj on 1/3/2016.
 */
public class GetFilmByIdResponse {

    @SerializedName("movie_results")
    private Film mFilm;

    public GetFilmByIdResponse() {
    }

    public Film getFilm() {
        return mFilm;
    }

    public void setFilm(Film film) {
        mFilm = film;
    }
}
