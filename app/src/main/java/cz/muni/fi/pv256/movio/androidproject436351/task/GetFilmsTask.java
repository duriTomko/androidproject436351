package cz.muni.fi.pv256.movio.androidproject436351.task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import cz.muni.fi.pv256.movio.androidproject436351.R;
import cz.muni.fi.pv256.movio.androidproject436351.model.Film;
import cz.muni.fi.pv256.movio.androidproject436351.model.serverresponse.GetFilmsResponse;
import cz.muni.fi.pv256.movio.androidproject436351.util.DateDeserializer;

/**
 * Created by Juraj on 11/15/2015.
 */
public class GetFilmsTask extends AsyncTask<Void, Void, ArrayList<Film>> {
    private static final String TAG = GetFilmsTask.class.getSimpleName();

    private Context mAppContext;
    private GetFilmsTaskListener mListener;

    public interface GetFilmsTaskListener {
        void onFilmsDownloaded(ArrayList<Film> filmList);
    }

    public GetFilmsTask(GetFilmsTaskListener listener, Context context) {
        mListener = listener;
        mAppContext = context.getApplicationContext();
    }

    @Override
    protected ArrayList<Film> doInBackground(Void... params) {
        if (isCancelled()) return null;

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("http://api.themoviedb.org/3/discover/movie" +
                        "?" + "api_key=" + mAppContext.getResources().getString(R.string.movie_db_api_key) +
                        "&year=1995")
                .build();

        Response response;
        try {
            response = client.newCall(request).execute();
            Log.d(TAG, "response = " + response.message());
            if (response.isSuccessful()) {
                return parseData(response.body().string());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<Film> films) {
        mListener.onFilmsDownloaded(films);
    }

    private ArrayList<Film> parseData(String jsonString) {
        if (jsonString == null) return null;
        Log.d(TAG, "jsonString = " + jsonString);

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Date.class, new DateDeserializer())
                .create();
        GetFilmsResponse response = gson.fromJson(jsonString, GetFilmsResponse.class);
        if (response != null) {
            return response.getFilmList();
        }
        return null;
    }
}
