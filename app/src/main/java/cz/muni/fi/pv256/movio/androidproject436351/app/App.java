package cz.muni.fi.pv256.movio.androidproject436351.app;

import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.StrictMode;

import cz.muni.fi.pv256.movio.androidproject436351.BuildConfig;
import cz.muni.fi.pv256.movio.androidproject436351.model.FilmLab;

/**
 * Created by Juraj on 9/21/2015.
 */
public class App extends Application {

    private FilmLab mFilmLab;
    private static Context sContext;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            initStrictMode();
        }
        mFilmLab = FilmLab.get();
        sContext = getApplicationContext();
    }

    private void initStrictMode() {
        StrictMode.ThreadPolicy.Builder tpb = new StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            tpb.penaltyFlashScreen();
        }
        StrictMode.setThreadPolicy(tpb.build());

        StrictMode.VmPolicy.Builder vmpb = new StrictMode.VmPolicy.Builder()
                .detectLeakedSqlLiteObjects()
                .penaltyLog();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            vmpb.detectLeakedClosableObjects();
        }
        StrictMode.setVmPolicy(vmpb.build());
    }

    public static Context getAppContext() {
        return sContext;
    }
}
