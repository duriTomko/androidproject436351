package cz.muni.fi.pv256.movio.androidproject436351.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import cz.muni.fi.pv256.movio.androidproject436351.app.App;
import cz.muni.fi.pv256.movio.androidproject436351.listener.DatabaseObserver;
import cz.muni.fi.pv256.movio.androidproject436351.model.Film;
import cz.muni.fi.pv256.movio.androidproject436351.util.DateUtil;

/**
 * Created by Juraj on 12/8/2015.
 */
public class DatabaseManager {
    private static final String TAG = DatabaseManager.class.getSimpleName();

    private static DatabaseManager sManager;
    private SQLiteDatabase mDatabase;
    private DatabaseObserver mObserver;

    private DatabaseManager() {
        mDatabase = new DatabaseHelper(App.getAppContext()).getWritableDatabase();
    }

    public static DatabaseManager getInstance() {
        if (sManager == null) {
            sManager = new DatabaseManager();
        }
        return sManager;
    }

    public void create(Film film) {
        if (film == null) return;
        Log.d(TAG, film.toString());

        ContentValues values = prepareValues(film);
        Film storedFilm = read(film.getId());
        if (storedFilm == null) {
            long id = mDatabase.insert(DbSchema.FilmTable.NAME, null, values);
            if (id == -1) {
                //throw new SQLException("Failed to insert item");
            } else {
                Log.d(TAG, "Film with id = " + id + ", was successfuly created");
                mObserver.onDatabaseDataChanged();
            }
        } else {
            update(film);
        }
    }

    public Film read(long id) {
        String clause = DbSchema.FilmTable.Cols.ID + "=?";
        String[] args = new String[] { String.valueOf(id) };
        Cursor cursor = mDatabase.query(DbSchema.FilmTable.NAME, null, clause, args, null, null, null);

        try {
            cursor.moveToFirst();
            if (cursor.isBeforeFirst() || cursor.isAfterLast()) {
                return null;
            }
            return retrieveFilm(cursor);

        } finally {
            cursor.close();
        }
    }

    public List<Film> readAll() {
        Cursor cursor = mDatabase.query(DbSchema.FilmTable.NAME, null, null, null, null, null, null, null);
        ArrayList<Film> filmList = new ArrayList<>();

        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                filmList.add(retrieveFilm(cursor));
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return filmList;
    }


    public void update(Film film) {
        String clause = DbSchema.FilmTable.Cols.ID + "=?";
        String[] args = new String[] { String.valueOf(film.getId()) };
        ContentValues values = prepareValues(film);
        int count = mDatabase.update(DbSchema.FilmTable.NAME, values, clause, args);
        if (count == 0) {
            throw new InvalidParameterException("Item with given id doesn't exist");
        } else {
            Log.d(TAG, "Film with id = " + film.getId() + ", was successfuly updated");
            mObserver.onDatabaseDataChanged();
        }
    }

    public void delete(long id) {
        String clause = DbSchema.FilmTable.Cols.ID + "=?";
        String[] args = new String[] { String.valueOf(id) };
        int count = mDatabase.delete(DbSchema.FilmTable.NAME, clause, args);
        if (count == 0) {
            throw new InvalidParameterException("Item with given id doesn't exist");
        } else {
            Log.d(TAG, "Film with id = " + id + ", was successfuly deleted");
            mObserver.onDatabaseDataChanged();
        }
    }

    private ContentValues prepareValues(Film film) {
        ContentValues values = new ContentValues();
        values.put(DbSchema.FilmTable.Cols.ID, film.getId());
        values.put(DbSchema.FilmTable.Cols.LANGUAGE, film.getLanguage());
        values.put(DbSchema.FilmTable.Cols.OVERVIEW, film.getOverview());
        values.put(DbSchema.FilmTable.Cols.POSTER_PATH, film.getPosterPath());
        values.put(DbSchema.FilmTable.Cols.RELEASE_DATE, DateUtil.dateToString(film.getReleaseDate()));
        values.put(DbSchema.FilmTable.Cols.TITLE, film.getTitle());
        values.put(DbSchema.FilmTable.Cols.BACKDROP_PATH, film.getBackdropPath());
        values.put(DbSchema.FilmTable.Cols.IS_FAVOURITE, film.getIsFavorite());
        return values;
    }

    private Film retrieveFilm(Cursor cursor) {
        long id = cursor.getLong(cursor.getColumnIndex(DbSchema.FilmTable.Cols.ID));
        String title = cursor.getString(cursor.getColumnIndex(DbSchema.FilmTable.Cols.TITLE));
        String releaseDate = cursor.getString(cursor.getColumnIndex(DbSchema.FilmTable.Cols.RELEASE_DATE));
        String posterPath = cursor.getString(cursor.getColumnIndex(DbSchema.FilmTable.Cols.POSTER_PATH));
        String language = cursor.getString(cursor.getColumnIndex(DbSchema.FilmTable.Cols.LANGUAGE));
        String overview = cursor.getString(cursor.getColumnIndex(DbSchema.FilmTable.Cols.OVERVIEW));
        String backdropPath = cursor.getString(cursor.getColumnIndex(DbSchema.FilmTable.Cols.BACKDROP_PATH));
        Boolean isFavourite = cursor.getInt(cursor.getColumnIndex(DbSchema.FilmTable.Cols.IS_FAVOURITE)) == 1 ? true : false;
        return new Film(id, DateUtil.stringToDate(releaseDate), posterPath, title, language, overview, backdropPath, isFavourite);
    }

    public void registerObserver(DatabaseObserver observer) {
        mObserver = observer;
    }

    public void unregisterObserver() {
        mObserver = null;
    }

    public boolean isRegisteredObserver() {
        return mObserver == null ? false : true;
    }
}
