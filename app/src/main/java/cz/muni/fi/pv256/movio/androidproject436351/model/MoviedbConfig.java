package cz.muni.fi.pv256.movio.androidproject436351.model;

/**
 * Created by Juraj on 11/30/2015.
 */
public class MoviedbConfig {

    private String baseUrl;

    private MoviedbConfig() {
    }



    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }
}
