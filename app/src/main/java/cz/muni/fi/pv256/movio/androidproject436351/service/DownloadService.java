package cz.muni.fi.pv256.movio.androidproject436351.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;

import cz.muni.fi.pv256.movio.androidproject436351.R;
import cz.muni.fi.pv256.movio.androidproject436351.client.RestApi;
import cz.muni.fi.pv256.movio.androidproject436351.model.serverresponse.GetFilmsResponse;
import cz.muni.fi.pv256.movio.androidproject436351.receiver.DownloadReceiver;
import cz.muni.fi.pv256.movio.androidproject436351.util.DateDeserializer;
import cz.muni.fi.pv256.movio.androidproject436351.util.NetworkUtil;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by Juraj on 11/30/2015.
 */
public class DownloadService extends IntentService {
    private static final String EXTRA_RECEIVER = "extra_receiver";
    public static final String EXTRA_FILMS = "extra_films";

    public static final int STATUS_OK = 0;
    public static final int STATUS_ERROR = 1;
    public static final int STATUS_NETWORK_NOT_AVAILABLE = 2;

    private static final String TAG = DownloadService.class.getSimpleName();
    private static final String API_URL = "http://api.themoviedb.org";

    private String mApi_key;

    private RestAdapter mAdapter;

    public static Intent newIntent(Context context, DownloadReceiver receiver) {
        Intent intent = new Intent(context, DownloadService.class);
        intent.putExtra(EXTRA_RECEIVER, receiver);
        return intent;
    }

    public DownloadService() {
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Date.class, new DateDeserializer())
                .create();

        mAdapter = new RestAdapter.Builder()
                .setEndpoint(API_URL)
                .setConverter(new GsonConverter(gson))
                .build();

        mApi_key = getResources().getString(R.string.movie_db_api_key);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        ResultReceiver receiver = intent.getParcelableExtra(EXTRA_RECEIVER);

        if (!NetworkUtil.isNetworkAvailable(this)) {
            receiver.send(STATUS_NETWORK_NOT_AVAILABLE, null);
            return;
        }

        RestApi methods = mAdapter.create(RestApi.class);
        GetFilmsResponse response = null;
        try {
            response = methods.getMovies(mApi_key);
        } catch (Exception e) {
            e.printStackTrace();
            receiver.send(STATUS_ERROR, null);
        }

        if (response != null) {
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(EXTRA_FILMS, response.getFilmList());
            receiver.send(STATUS_OK, bundle);
        } else {
            receiver.send(STATUS_ERROR, null);
        }
    }
}
