package cz.muni.fi.pv256.movio.androidproject436351.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import cz.muni.fi.pv256.movio.androidproject436351.app.App;

/**
 * Created by Juraj on 10/18/2015.
 */
public class NetworkUtil {
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connMgr =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connMgr.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        }
        return false;
    }

    public static boolean isNetworkAvailable() {
        return isNetworkAvailable(App.getAppContext());
    }
}
