package cz.muni.fi.pv256.movio.androidproject436351.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import cz.muni.fi.pv256.movio.androidproject436351.fragment.DiscoverFragment;
import cz.muni.fi.pv256.movio.androidproject436351.fragment.FavoritesFragment;

/**
 * Created by Juraj on 1/2/2016.
 */
public class MainActivityPagerAdapter extends FragmentPagerAdapter {
    private static final int pagesNumber = 2;
    private CharSequence[] mTitles;

    public MainActivityPagerAdapter(FragmentManager fm, CharSequence[] titles) {
        super(fm);
        mTitles = titles;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 1:
                return new FavoritesFragment();

            case 0:
            default:
                return new DiscoverFragment();

        }
    }

    @Override
    public int getCount() {
        return pagesNumber;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles[position];
    }
}
