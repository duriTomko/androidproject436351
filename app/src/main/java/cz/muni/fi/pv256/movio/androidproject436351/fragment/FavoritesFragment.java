package cz.muni.fi.pv256.movio.androidproject436351.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import cz.muni.fi.pv256.movio.androidproject436351.fragment.base.MainFragment;
import cz.muni.fi.pv256.movio.androidproject436351.loader.FilmLoader;
import cz.muni.fi.pv256.movio.androidproject436351.model.Film;

/**
 * Created by Juraj on 1/2/2016.
 */
public class FavoritesFragment extends MainFragment implements LoaderManager.LoaderCallbacks<List<Film>> {
    private static final String TAG = FavoritesFragment.class.getSimpleName();
    private static final int LOADER_ID = 1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LoaderManager lm = getActivity().getSupportLoaderManager();
        lm.initLoader(LOADER_ID, null, this);
    }

    @Override
    public Loader<List<Film>> onCreateLoader(int id, Bundle args) {
        return new FilmLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<List<Film>> loader, List<Film> data) {
        if (data == null) return;
        Log.d(TAG, "onLoadFinished(), data = " + data.toString());
        updateAdapterData(new ArrayList<>(data));
    }

    @Override
    public void onLoaderReset(Loader<List<Film>> loader) {
        updateAdapterData(null);
    }
}
