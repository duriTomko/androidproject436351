package cz.muni.fi.pv256.movio.androidproject436351.model.serverresponse;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import cz.muni.fi.pv256.movio.androidproject436351.model.Film;

/**
 * Created by Juraj on 11/15/2015.
 */
public class GetFilmsResponse {

    @SerializedName("page")
    private int page;

    @SerializedName("results")
    private ArrayList<Film> filmList;

    @SerializedName("total_pages")
    private int totalPages;

    @SerializedName("total_results")
    private int totalResults;

    public GetFilmsResponse() {
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public ArrayList<Film> getFilmList() {
        return filmList;
    }

    public void setFilmList(ArrayList<Film> filmList) {
        this.filmList = filmList;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }
}
