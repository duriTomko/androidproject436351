package cz.muni.fi.pv256.movio.androidproject436351.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Juraj on 10/18/2015.
 */
public class Film implements Parcelable {

    @SerializedName("id")
    private Long mId;

    @SerializedName("release_date")
    private Date mReleaseDate;

    @SerializedName("poster_path")
    private String mPosterPath;

    @SerializedName("title")
    private String mTitle;

    @SerializedName("original_language")
    private String mLanguage;

    @SerializedName("overview")
    private String mOverview;

    @SerializedName("backdrop_path")
    private String mBackdropPath;

    private Boolean mIsFavorite = false;

    public Film() {
    }

    public Film(long id, Date releaseDate, String posterPath, String title, String language, String overview,
                String backdropPath, boolean isFavorite) {
        mId = id;
        mReleaseDate = releaseDate;
        mPosterPath = posterPath;
        mTitle = title;
        mLanguage = language;
        mOverview = overview;
        mBackdropPath = backdropPath;
        mIsFavorite = isFavorite;
    }

    public String getOverview() {
        return mOverview;
    }

    public void setOverview(String overview) {
        mOverview = overview;
    }

    public String getLanguage() {
        return mLanguage;
    }

    public void setLanguage(String language) {
        mLanguage = language;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getPosterPath() {
        return mPosterPath;
    }

    public void setPosterPath(String posterPath) {
        mPosterPath = posterPath;
    }

    public Date getReleaseDate() {
        return mReleaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        mReleaseDate = releaseDate;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getBackdropPath() {
        return mBackdropPath;
    }

    public void setBackdropPath(String backdropPath) {
        mBackdropPath = backdropPath;
    }

    public Boolean getIsFavorite() {
        return mIsFavorite;
    }

    public void setIsFavorite(Boolean isFavorite) {
        mIsFavorite = isFavorite;
    }

    @Override
    public String toString() {
        return "id = " + mId + ";" +
                "release date = " + mReleaseDate + ";" +
                "cover path = " + mPosterPath + ";" +
                "title = " + mTitle + ";" +
                "language = " + mLanguage + ";" +
                "overview = " + mOverview + ";" +
                "isFavourite = " + mIsFavorite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (!(o instanceof Film)) {
            return false;
        }
        Film other = (Film) o;
        if (getId() == null) {
            if (other.getId() != null) {
                return false;
            }
        } else if (!getId().equals(other.getId())) {
            return false;
        }
        if (getReleaseDate() == null) {
            if (other.getReleaseDate() != null) {
                return false;
            }
        } else if (!getReleaseDate().equals(other.getReleaseDate())) {
            return false;
        }
        if (getPosterPath() == null) {
            if (other.getPosterPath() != null) {
                return false;
            }
        } else if (!getPosterPath().equals(other.getPosterPath())) {
            return false;
        }
        if (getTitle() == null) {
            if (other.getTitle() != null) {
                return false;
            }
        } else if (!getTitle().equals(other.getTitle())) {
            return false;
        }
        if (getLanguage() == null) {
            if (other.getLanguage() != null) {
                return false;
            }
        } else if (!getLanguage().equals(other.getLanguage())) {
            return false;
        }
        if (getOverview() == null) {
            if (other.getOverview() != null) {
                return false;
            }
        } else if (!getOverview().equals(other.getOverview())) {
            return false;
        }
        if (getBackdropPath() == null) {
            if (other.getBackdropPath() != null) {
                return false;
            }
        } else if (!getBackdropPath().equals(other.getBackdropPath())) {
            return false;
        }
        if (getIsFavorite() == null) {
            if (other.getIsFavorite() != null) {
                return false;
            }
        } else if (!getIsFavorite().equals(other.getIsFavorite())) {
            return false;
        }

        return true;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public Film(Parcel in) {
        mId = in.readLong();
        mReleaseDate = new Date(in.readLong());
        mPosterPath = in.readString();
        mTitle = in.readString();
        mLanguage = in.readString();
        mOverview = in.readString();
        mBackdropPath = in.readString();
        mIsFavorite = in.readInt() == 1 ? true : false;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mId);
        dest.writeLong(mReleaseDate.getTime());
        dest.writeString(mPosterPath);
        dest.writeString(mTitle);
        dest.writeString(mLanguage);
        dest.writeString(mOverview);
        dest.writeString(mBackdropPath);
        dest.writeInt(mIsFavorite == true ? 1 : 0);
    }

    public static final Parcelable.Creator<Film> CREATOR = new Parcelable.Creator<Film>() {
        @Override
        public Film createFromParcel(Parcel source) {
            return new Film(source);
        }

        @Override
        public Film[] newArray(int size) {
            return new Film[size];
        }
    };
}
