package cz.muni.fi.pv256.movio.androidproject436351.database;

/**
 * Created by Juraj on 12/8/2015.
 */
public class DbSchema {

    public static class FilmTable {
        public static final String NAME = "film_table";

        public static final class Cols {
            public static final String ID = "_id";
            public static final String RELEASE_DATE = "release_date";
            public static final String POSTER_PATH = "poster_path";
            public static final String TITLE = "title";
            public static final String LANGUAGE = "language";
            public static final String OVERVIEW = "overview";
            public static final String BACKDROP_PATH = "backdrop_path";
            public static final String IS_FAVOURITE = "is_favourite";
        }
    }
}
