package cz.muni.fi.pv256.movio.androidproject436351.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import cz.muni.fi.pv256.movio.androidproject436351.BuildConfig;
import cz.muni.fi.pv256.movio.androidproject436351.R;
import cz.muni.fi.pv256.movio.androidproject436351.model.Film;

/**
 * Created by Juraj on 10/18/2015.
 */
public class FilmAdapter extends BaseAdapter  {
    private static final String TAG = FilmAdapter.class.getSimpleName();

    private Fragment mFragment;
    private ArrayList<Film> mFilmList;
    private FilmAdapterListener mListener;

    public interface FilmAdapterListener {
        void onItemClick(Film film);
        void onItemLongClick(Film film);
    }

    public FilmAdapter(ArrayList<Film> filmList, Fragment fragment, FilmAdapterListener listener) {
        mFilmList = filmList;
        mFragment = fragment;
        mListener = listener;
    }

    public void setData(ArrayList<Film> filmList) {
        mFilmList = filmList;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mFilmList.size();
    }

    @Override
    public Object getItem(int position) {
        return mFilmList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            if (BuildConfig.logging) {
                Log.i(TAG, "inflate radku " + position);
            }

            convertView = LayoutInflater.from(mFragment.getActivity()).inflate(R.layout.grid_item_film, parent, false);
            ViewHolder holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }
        if (BuildConfig.logging) {
            Log.i(TAG, "recyklace radku " + position);
        }

        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.bindView(mFilmList.get(position));

        return convertView;
    }


    private class ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private ImageView coverIv;
        private Film film;

        public ViewHolder(View view) {
            coverIv = (ImageView) view.findViewById(R.id.grid_item_film_cover);
        }

        public void bindView(Film film) {
            if (film == null) return;
            this.film = film;

            String url = "http://image.tmdb.org/t/p/" + "w342/" + film.getPosterPath();
            Picasso.with(mFragment.getActivity())
                    .load(url)
                    .into(coverIv);

            coverIv.setOnClickListener(this);
            coverIv.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mListener.onItemClick(film);
        }

        @Override
        public boolean onLongClick(View v) {
            mListener.onItemLongClick(film);
            return true;
        }
    }
}
