package cz.muni.fi.pv256.movio.androidproject436351.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import java.util.List;

import cz.muni.fi.pv256.movio.androidproject436351.database.DatabaseManager;
import cz.muni.fi.pv256.movio.androidproject436351.listener.DatabaseObserver;
import cz.muni.fi.pv256.movio.androidproject436351.model.Film;

/**
 * Created by Juraj on 1/2/2016.
 */
public class FilmLoader extends AsyncTaskLoader<List<Film>> implements DatabaseObserver {
    private static final String TAG = FilmLoader.class.getSimpleName();

    private List<Film> mData;
    private DatabaseManager mDbManager;

    public FilmLoader(Context context) {
        super(context);
        mDbManager = DatabaseManager.getInstance();
    }

    @Override
    public List<Film> loadInBackground() {
        Log.d(TAG, "loadInBackground() called");

        return mDbManager.readAll();
    }

    @Override
    public void deliverResult(List<Film> data) {
        Log.d(TAG, "deliverResult() called");

        if (isReset()) {
            releaseResources(data);
            return;
        }

        List<Film> oldData = mData;
        mData = data;

        if (isStarted()) {
            super.deliverResult(data);
        }

        if (oldData != null && oldData != data) {
            releaseResources(oldData);
        }
    }

    @Override
    protected void onStartLoading() {
        Log.d(TAG, "onStartLoading() called");
        super.onStartLoading();
        if (mData != null) {
            deliverResult(mData);
        }

        if (!mDbManager.isRegisteredObserver()) {
            mDbManager.registerObserver(this);
        }

        if (takeContentChanged() || mData == null) {
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        Log.d(TAG, "onStopLoading() called");
        cancelLoad();
    }

    @Override
    protected void onReset() {
        Log.d(TAG, "onReset() called");
        super.onReset();
        onStopLoading();

        if (mData != null) {
            releaseResources(mData);
        }

        if (mDbManager.isRegisteredObserver()) {
            mDbManager.unregisterObserver();
        }
    }

    @Override
    public void onCanceled(List<Film> data) {
        Log.d(TAG, "onCanceled() called");
        super.onCanceled(data);
        releaseResources(data);
    }

    @Override
    public void onDatabaseDataChanged() {
        Log.d(TAG, "onDatabaseDataChanged() called");
        onContentChanged();
    }

    private void releaseResources(List<Film> data) {
        data = null;
    }
}
