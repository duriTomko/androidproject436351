package cz.muni.fi.pv256.movio.androidproject436351.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import cz.muni.fi.pv256.movio.androidproject436351.R;
import cz.muni.fi.pv256.movio.androidproject436351.database.DatabaseManager;
import cz.muni.fi.pv256.movio.androidproject436351.model.Film;
import cz.muni.fi.pv256.movio.androidproject436351.model.FilmLab;
import cz.muni.fi.pv256.movio.androidproject436351.util.DateUtil;

/**
 * Created by Juraj on 10/26/2015.
 */
public class FilmDetailFragment extends Fragment {
    private static final String TAG = FilmDetailFragment.class.getSimpleName();

    public static final String ARGS_FILM = "args_film";
    public static final String BUNDLE_IS_FAVORITE = "bundle_is_favorite";

    private static final String POSTER_SIZE = "w342";
    private static final String BACKDROP_SIZE = "w780";

    private Context mContext;
    private Film mFilm;
    private FilmLab mFilmLab;
    private DatabaseManager mDatabaseManager;
    private boolean mIsFavorite = false;

    public static FilmDetailFragment newInstance(Film film) {
        FilmDetailFragment fragment = new FilmDetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARGS_FILM, film);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initData(savedInstanceState);
        mDatabaseManager = DatabaseManager.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_film_detail, container, false);
        setToolbar(view);
        initWidgets(view);
        return view;
    }

    private void initData(Bundle bundle) {
        mContext = getActivity();
        mFilm = getArguments().getParcelable(ARGS_FILM);
        mFilmLab = FilmLab.get();

        if (bundle != null) {
            mIsFavorite = bundle.getBoolean(BUNDLE_IS_FAVORITE);
            mFilm.setIsFavorite(mIsFavorite);
        }

        Log.d(TAG, "initData(), film = " + mFilm.toString());
    }

    private void setToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle(getString(R.string.title_film_detail_fragment));
    }

    private void initWidgets(View view) {
        TextView titleTv = (TextView) view.findViewById(R.id.fragment_film_detail_title);
        ImageView coverIv = (ImageView) view.findViewById(R.id.fragment_film_detail_image_view);
        final FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);

        titleTv.setText(mFilm.getTitle());
        setImage(coverIv, mFilm.getPosterPath(), POSTER_SIZE);

        fab.setImageResource(mFilm.getIsFavorite() ? R.drawable.ic_star_full : R.drawable.ic_star_empty);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIsFavorite = !mFilm.getIsFavorite();
                mFilmLab.setFavorite(mFilm.getId(), mIsFavorite);
                mFilm.setIsFavorite(mIsFavorite);

                fab.setSelected(mIsFavorite);
                fab.setImageResource(mIsFavorite ? R.drawable.ic_star_full : R.drawable.ic_star_empty);

                if (mIsFavorite) {
                    mDatabaseManager.create(mFilm);
                } else {
                    mDatabaseManager.delete(mFilm.getId());
                }
            }
        });

        ImageView backdropIv = (ImageView) view.findViewById(R.id.backdrop_image);
        setImage(backdropIv, mFilm.getBackdropPath(), BACKDROP_SIZE);

        TextView overview = (TextView) view.findViewById(R.id.overview);
        overview.setText(mFilm.getOverview());

        TextView language = (TextView) view.findViewById(R.id.language);
        language.setText(mFilm.getLanguage());

        TextView releaseDate = (TextView) view.findViewById(R.id.release_date);
        releaseDate.setText(DateUtil.formatDate(mFilm.getReleaseDate()));
    }

    private void setImage(ImageView coverIv, String path, String format) {
        String url = "http://image.tmdb.org/t/p/" + format + "/" + path;
        Picasso.with(mContext)
                .load(url)
                .into(coverIv);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(BUNDLE_IS_FAVORITE, mIsFavorite);
    }
}
